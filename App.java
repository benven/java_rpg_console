import java.util.Scanner;

public class App {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);


        String name_hero = null;
        int tour = 0;
        byte cmd;
        boolean end = false;
        
       System.out.print("Quel nom pour votre héro? : ");
       name_hero = sc.nextLine();
       Guerrier joueur = new Guerrier(name_hero);

       
       do {
           System.out.println("Que voulez-vous faire? : ");
           System.out.println("1.visiting forest 2.informations sur le héro");
           cmd = sc.nextByte();
           
           switch (cmd) {
               case 1: //Si le joueur choisit la forêt, on instancie un objet foret et on applique la méthode visiter.
                Forest forest = new Forest(joueur);
                forest.visiting_forest();
               break;
               case 2:
                 joueur.informations_hero(joueur.nom_hero);
               break;
            }
           
       } while(end == false);



    }


}