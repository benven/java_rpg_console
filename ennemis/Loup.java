public class Loup extends Ennemy{
    public Loup(){
        name = "Wolf";
        cri_monstre = "Ahouuuuhouuuuu";
        level = 4;
        alive = true;
        life = 7;
        exp_gain = 30;
        puissance = level * 1.23;
    }
}
