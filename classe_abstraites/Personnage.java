abstract public class Personnage {
    public String mName;
    public int mLevel;
    public int mLife;
    public int mExperiences_points;
    public double mPuissance;
    public String nom_hero;
    
    //attaque normale : tous les héros la possède
    public void attaque_normale(Ennemy ennemi[], int rand){
        System.out.println("\tVous tranchez la chair du " + ennemi[rand].name);
        System.out.println("\t" + ennemi[rand].name + " n'a plus que " + ennemi[rand].life + " points de vie.");
        ennemi[rand].life -= this.mPuissance;
    }

    public void gestion_niveau(Personnage joueur, int xpJoueur){

        if(xpJoueur > 0 && xpJoueur <= 10){
            joueur.mLevel = 1;
        } else if (xpJoueur > 10 && xpJoueur <= 30) {
            joueur.mLevel = 2;
        } else if (xpJoueur > 30 && xpJoueur <= 50) {
            joueur.mLevel = 3;
        } else if (xpJoueur > 50 && xpJoueur <=90) {
            joueur.mLevel = 4;
        } else if (xpJoueur > 90 && xpJoueur <= 130) {
            joueur.mLevel = 5;
        } else if (xpJoueur > 130 && xpJoueur <= 200) {
            joueur.mLevel = 6;
        } else if (xpJoueur > 200 && xpJoueur <= 300) {
            joueur.mLevel = 7;
        } else if (xpJoueur > 300 && xpJoueur <= 700) {
            joueur.mLevel = 8;
        } else if (xpJoueur > 700 && xpJoueur <= 900) {
            joueur.mLevel = 9;
        }
        //la puissance de combat se définit à travers le niveau
        joueur.mPuissance = joueur.mLevel * 2;
    }
    
    /* Méthode pour donner les informations du personnages */
    public void informations_hero(String joueur){
        System.out.println("*******************************");
        System.out.println("Informations de " + this.mName);
        System.out.println("Expériences : " + this.mExperiences_points);
        System.out.println("Niveau : " + this.mLevel);
        System.out.println("Puissance : " + this.mPuissance);
        System.out.println("Classe : " + this.nom_hero);
        System.out.println("Vie : " + this.mLife);
        System.out.println("*******************************");
    }
}

