import java.util.Scanner; 
import java.util.Random;


public class Forest {
    /*déclaration des variables*/
    Guerrier joueur;
    byte cmd;
    private int rand;
    Scanner sc = new Scanner(System.in);

    Ennemy[] ennemi = new Ennemy[2];//création d'un tableau d'ennemi 
    
    
    //constructeur, le joueur est passé en paramètre
    public Forest(Guerrier joueur) {
       
       this.joueur = joueur;
    }
    //méthode de visite
    public void visiting_forest() 
    {
        //on place un ennemi dans une case du tableau
        ennemi[0] = new Rat(); 
        ennemi[1] = new Loup();
     
        /*cmd = 0;*/
        rand = new Random().nextInt(2); //on crée un numéro random entre 0 et 1
        
        //Un ennemi pop
        System.out.println("\n[ALERTE] Un " + ennemi[rand].name + " apparaît! " + ennemi[rand].cri_monstre);
      
        

        while (ennemi[rand].alive == true) //tant que l'ennemi est vivant. 
        {
         if(joueur instanceof Guerrier){ //menu spécifique si le joueur est guerrier
          joueur.menu_guerrier(joueur);
         }
          cmd = sc.nextByte();

         
          
          if(joueur instanceof Guerrier){ //menu spécifique si le joueur est guerrier
            joueur.attaques_guerrier(cmd, joueur, ennemi, rand);
           }
          

          /* tour de l'ennemi */
          ennemi[rand].attack(joueur);
          System.out.println("Aie! L'immonde " + ennemi[rand].name + " vous inflige " + ennemi[rand].puissance + "Dégats! " );
          System.out.println("La vie de votre héro passe désormais à : " + joueur.mLife);

          if(ennemi[rand].life <= 0)
          {
              ennemi[rand].alive = false;
              System.out.println("Gagné!");
              joueur.mExperiences_points += ennemi[rand].exp_gain;
              joueur.gestion_niveau( joueur ,joueur.mExperiences_points);
              System.out.println("En tuant " + ennemi[rand].name + " vous obtenez " + ennemi[rand].exp_gain + " xp!");
              System.out.println(joueur.mName + " possède désormais " + joueur.mExperiences_points + " xp");
          }

    

        }
     } /*else {
        System.out.println("Vous vous baladez paisiblement sans rencontrer de monstre, et trouvez un trésor! ");
        System.out.print(("Vous obtenez 3 points d'expériences. "));
        joueur.mExperiences_points += 3;*/

     
    
}