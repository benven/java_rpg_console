public class Guerrier extends Personnage{
    //constructeur du guerrier
    public Guerrier(String name){
        mName = name;
        mLevel = 1;
        mLife = 120;
        mExperiences_points = 0;
        mPuissance = (mLevel * 1.24);
        nom_hero = "Un noble guerrier.";
    }
    /*****Attaque spéciale *********/
    public void attaque_flamboyante(Ennemy e){
        System.out.println(" Attaque flamboyante! " + e.name + " pousse un cri strident!");
        e.life -= (mPuissance * 2);
    }

    public void menu_guerrier(Personnage joueur){
        System.out.println("************************************");
        System.out.println("\tC'est le tour de " + joueur.mName);
        System.out.println("\tAction  : ");
        System.out.println("\t1. Attaquer");
        System.out.println("\t2. Attaque flamboyante");
        System.out.println("************************************");
    }

    public void attaques_guerrier(int cmd, Personnage joueur, Ennemy ennemi[],int rand) {
        switch(cmd) 
        {
            case 1:  
            joueur.attaque_normale(ennemi, rand);
            break;
            case 2:
            this.attaque_flamboyante(ennemi[rand]);
            break;
        }

    }



   

}
